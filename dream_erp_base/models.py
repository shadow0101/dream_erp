from django.db import models

# Create your models here.


class Customer(models.Model):
    """
    Blueprint of the Customer
    """

    # Customer fields

    name = models.CharField(
        verbose_name='Customer name',
        max_length=255,
        blank=False,
        help_text="Enter a customer's name",
    )
    company = models.CharField(
        verbose_name='Company',
        max_length=255,
        blank=True,
    )
    street = models.CharField(
        verbose_name='Street',
        max_length=255,

    )
    city = models.CharField(
        verbose_name='City',
        max_length=255,

    )
    zip_code = models.CharField(
        verbose_name='Zip',
        max_length=255,

    )
    country = models.CharField(
        verbose_name='Country',
        default='Philippines',
        max_length=255,

    )
    phone = models.CharField(
        verbose_name='Phone',
        max_length=255,

    )
    mobile = models.CharField(
        verbose_name='Mobile',
        max_length=255,

    )
    fax = models.CharField(
        verbose_name='Fax',
        max_length=255,

    )
    email = models.EmailField(
        verbose_name='Email',
        max_length=255,
    )

    def __str__(self):
        return self.name


class Location(models.Model):
    """
    Blueprint of the Location model
    """

    # Location fields

    name = models.CharField(
        verbose_name='Location name',
        max_length=255,
        blank=False,
        help_text="Enter a location name",
    )
    active = models.BooleanField(
        verbose_name='Active',
        default=True,
        help_text="Is active?",
        blank=False,
    )

    def __str__(self):
        return self.name