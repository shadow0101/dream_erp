from django.contrib import admin
from .models import Customer,Location
from django.contrib.admin import AdminSite


# Register your models here.


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'city', 'country', 'phone')
    fieldsets = (
        (None, {
            'fields': [('name', 'company')],
        }),
        ('Address', {
            'fields': [('street', 'city'), ('zip_code', 'country')]
        }),
        ('Contact Information', {
            'fields': [('phone', 'mobile'), ('fax', 'email')]
        })
    )


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Location)
admin.site.site_header = 'Dream ERP'

