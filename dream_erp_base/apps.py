from django.apps import AppConfig


class DreamErpBaseConfig(AppConfig):
    name = 'dream_erp_base'
    label = 'Base'

