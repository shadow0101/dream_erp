# Dream ERP

Enterprise resource planning (ERP) is the integrated management of core business processes, often in real-time and mediated by software and technology.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Django 2.0.2
Python 3.5.2
Xampp
mysql-connector(windows)
mysql-connector(linux)
pip

```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
No content
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Django](https://www.djangoproject.com) - The web framework used


## Contributing


## Versioning


## Authors


## License


## Acknowledgments



