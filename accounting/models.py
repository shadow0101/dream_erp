from django.db import models
# from django.apps import apps
# Create your models here.


class Asset(models.Model):
    """
    Blueprint of the Asset model
    """

    # Location = apps.get_model('inventory', 'Location')

    # Asset fields

    name = models.CharField(
        verbose_name='Asset name',
        max_length=255,
        help_text="Enter a Asset's name",
    )
    category = models.ForeignKey(
        'Category',
        verbose_name='Category',
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
        help_text="Select the asset's category"
    )
    date = models.DateField(
        verbose_name='Date',
        blank=False,
    )
    gross_value = models.FloatField(
        verbose_name='Gross Value',
        default=0.0,
    )
    salvage_value = models.FloatField(
        verbose_name='Salvage Value',
        default=0.0,
    )
    location = models.ForeignKey(
        'Base.Location',
        verbose_name='Location',
        on_delete=models.SET_NULL,
        null=True,
    )
    depreciable_cost = models.FloatField(
        verbose_name='Depreciable Cost',
        default=0.0,
    )
    life = models.IntegerField(
        verbose_name="Asset's Lifespan",
        default=0,
    )
    depreciation_year = models.FloatField(
        verbose_name='Depreciation/Year',
        default=0.0,
    )

    def __str__(self):
        return self.name


class Depreciation(models.Model):
    """
    Blueprint for Depreciation model
    """

    asset_id = models.ForeignKey(
        'Asset',
        on_delete=models.SET_NULL,
        null=True,

    )
    depreciation_value = models.FloatField(
        verbose_name='Depreciation Value',
        max_length=255,
    )
    annual_depreciation = models.FloatField(
        verbose_name='Annual Depreciation',
        max_length=255,
    )
    depreciation_rate = models.FloatField(
        verbose_name='Rate',
        max_length=255,
    )
    depreciation_date = models.DateField(
        verbose_name='Date',
        max_length=255,
    )


class Category(models.Model):
    # Category fields

    name = models.CharField(
        verbose_name='Category',
        max_length=255,

    )

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name
