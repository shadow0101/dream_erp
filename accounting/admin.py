from django.contrib import admin
from .models import Asset,Category,Depreciation
from django.conf.urls import url
from django.shortcuts import render

# Register your models here.


class AssetDepreciationInline(admin.TabularInline):
    model = Depreciation
    extra = 0


class AssetAdmin(admin.ModelAdmin):
    class Media:
        js = (
            'js/asset.js',
        )
    fieldsets = (
        (None, {
            'fields': [('name','gross_value'),
                       ('category','salvage_value'),
                       ('date','location'),('depreciable_cost','life','depreciation_year')],
            'classes': 'form-row-6columns',
        }),
    )
    inlines = [AssetDepreciationInline,]
    # readonly_fields = ('depreciable_cost','depreciation_year',)
    # comment muna kasi walang id yung fields;

    # def get_urls(self):
    #     urls = super(AssetAdmin, self).get_urls()
    #     my_urls = [url(r"^compute_depreciation/$", self.compute_depreciation, name='compute_depreciation')]
    #     return my_urls + urls
    #
    # def compute_depreciation(self,request):
    #     return render(request,context={})



admin.site.register(Asset,AssetAdmin)
admin.site.register(Category)
admin.site.register(Depreciation)
