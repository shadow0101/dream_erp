if (!$) {
    $ = django.jQuery;
}

$(function(){
    var gross_field = document.getElementById("id_gross_value");
    var salvage_field = document.getElementById("id_salvage_value");
    var cost_field = document.getElementById("id_depreciable_cost");
    $('#id_salvage_value').change(function(){
        cost_field.value = compute_depreciable_cost(gross_field.value,salvage_field.value)
    });
    $("#id_gross_value").change(function(){
        cost_field.value = compute_depreciable_cost(gross_field.value,salvage_field.value)
    });

    // For depreciation computation
    // TODO: all data(fields) are hardcoded change this function if you add new fields or etc.
    $("#compute_depreciation").click(function(){
        // id = id_depreciation_set-<index>-depreciation_value
        var id_value = "id_depreciation_set-{0}-depreciation_value";
        var id_annual = "id_depreciation_set-{0}-annual_depreciation";
        var id_rate = "id_depreciation_set-{0}-depreciation_rate";
        var id_date = "id_depreciation_set-{0}-depreciation_date";
        var counter = 0;
        var formatted_id_value,formatted_id_annual,formatted_id_rate,formatted_id_date;
        var value,annual,rate,date;
        while(counter!=3){
            formatted_id_value= format(id_value,[counter]);
            formatted_id_annual= format(id_annual,[counter]);
            formatted_id_rate = format(id_rate,[counter]);
            formatted_id_date = format(id_date,[counter]);



            value=document.getElementById(formatted_id_value);
            annual=document.getElementById(formatted_id_annual);
            rate=document.getElementById(formatted_id_rate);
            date=document.getElementById(formatted_id_date);
            value.value = 123;
            annual.value = 123;
            rate.value= 123;
            date.value= 123;
            counter++;
        }
    });
});
function compute_depreciable_cost(x,y){
    return x -y
}

function format(source, params) {
    $.each(params,function (i, n) {
        source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
    })
    return source;
}

