from django.contrib import admin
from .models import Product


# from dream_erp_base.models import Customer

# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'cost', 'type', 'barcode', 'location',)
    # fields = ['name','type',('price','cost'),'barcode','location']
    list_filter = ('price', 'cost')
    fieldsets = (
        (None, {
            'fields': ('name',),
        }),
        ('General Information', {
            'fields': ('type', 'price', 'cost', 'barcode', 'location')
        })
    )


admin.site.register(Product, ProductAdmin)
