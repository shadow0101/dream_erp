from django.db import models


# Create your models here.


class Product(models.Model):
    """
    Blueprint of the Product model
    """

    # Model fields

    name = models.CharField(
        max_length=255,
        verbose_name='Product name',
        help_text="Enter a product name",
        blank=False,
    )
    PRODUCT_TYPE = (
        ('consuma', 'Consumable'),
        ('service', 'Service'),
        ('product', 'Stockable Product')
    )
    type = models.CharField(
        verbose_name='Product Type',
        max_length=7,
        choices=PRODUCT_TYPE,
        blank=False,
        help_text="Select a type for this product"
    )
    barcode = models.CharField(
        verbose_name='Barcode',
        max_length=255,
        help_text="Enter the product's barcode",
        blank=True,
    )
    price = models.FloatField(
        verbose_name='Sale Price',
        help_text="Enter the product's price",
        blank=False,
        default=0.0,
    )
    cost = models.FloatField(
        verbose_name='Cost',
        help_text="Enter the product's cost",
        blank=False,
        default=0.0,
    )
    location = models.ForeignKey(
        'Base.Location',
        verbose_name='Location',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text="Select the product's location",
    )

    def __str__(self):
        return self.name



